DROP DATABASE IF EXISTS bibliotheque;

CREATE DATABASE bibliotheque;
USE bibliotheque;

CREATE TABLE TypeArticle
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    libelle VARCHAR(100),
    empruntable TINYINT(1)

) ENGINE = InnoDB;

CREATE TABLE Article
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    libelle VARCHAR(100),
    reference VARCHAR(100),
    disponible TINYINT(1),
    typeArticle INT,
    CONSTRAINT fk_type_Article FOREIGN KEY (typeArticle) REFERENCES TypeArticle(id)

) ENGINE = InnoDB;

-------------------------------------

DROP DATABASE IF EXISTS bibliotheque;

CREATE DATABASE bibliotheque;
USE bibliotheque;
DROP TABLE IF EXISTS article ;
CREATE TABLE article (
    id_Article BIGINT AUTO_INCREMENT NOT NULL,
    libelle_Article VARCHAR(255), reference_Article VARCHAR(255),
    disponible_Article TINYINT(1),
    id_typeArticle BIGINT,
     PRIMARY KEY (id_Article)
                     ) ENGINE=InnoDB;
DROP TABLE IF EXISTS valueCaracteristique ;
CREATE TABLE valueCaracteristique (
    id_valueCaracteristique BIGINT AUTO_INCREMENT NOT NULL,
     Value_valueCaracteristique VARCHAR(255),
     id_caracteristique BIGINT,
      id_Article BIGINT  ,
      PRIMARY KEY (id_valueCaracteristique)) ENGINE=InnoDB;
DROP TABLE IF EXISTS typeArticle ;
CREATE TABLE typeArticle (
    id_typeArticle BIGINT AUTO_INCREMENT NOT NULL,
    libelle_typeArticle VARCHAR(255),
    empruntable_typeArticle TINYINT(1),
    PRIMARY KEY (id_typeArticle)) ENGINE=InnoDB;
DROP TABLE IF EXISTS caracteristique ;
CREATE TABLE caracteristique (
    id_caracteristique BIGINT AUTO_INCREMENT NOT NULL,
    libelle_caracteristique VARCHAR(255),
    id_typeArticle BIGINT,
    PRIMARY KEY (id_caracteristique)
                             ) ENGINE=InnoDB;
ALTER TABLE article ADD CONSTRAINT FK_article_id_typeArticle FOREIGN KEY (id_typeArticle) REFERENCES typeArticle (id_typeArticle);
ALTER TABLE valueCaracteristique ADD CONSTRAINT FK_valueCaracteristique_id_caracteristique FOREIGN KEY (id_caracteristique) REFERENCES caracteristique (id_caracteristique);
ALTER TABLE valueCaracteristique ADD CONSTRAINT FK_valueCaracteristique_id_Article FOREIGN KEY (id_Article) REFERENCES article (id_Article);
ALTER TABLE caracteristique ADD CONSTRAINT FK_caracteristique_id_typeArticle FOREIGN KEY (id_typeArticle) REFERENCES typeArticle (id_typeArticle);

-----------------------------------------------

INSERT INTO article (libelle_Article,reference_Article,disponible_Article,id_typeArticle)
VALUES ('Jul','56842',1,1);
INSERT INTO valuecaracteristique (Value_valuecaracteristique,id_caracteristique,id_Article)
VALUES ('3h00',1,1);
INSERT INTO valuecaracteristique (Value_valuecaracteristique,id_caracteristique,id_Article)
VALUES ('LigaOne',2,1);

