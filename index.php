<?php

require_once 'vendor/autoload.php';
require_once 'mvc/utils/router.php';

use mvc\utils\router;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;



$alreadyCaught = false;
function isErrorCatchable($errno) {
    $ignore =
        // Warnings
        E_WARNING | E_CORE_WARNING | E_COMPILE_WARNING | E_USER_WARNING |
        // Notices
        E_NOTICE | E_USER_NOTICE |
        // Strict coding
        E_STRICT | E_DEPRECATED | E_USER_DEPRECATED | E_RECOVERABLE_ERROR;
    return  (error_reporting() != 0 && !($errno & $ignore));
}
function getErrorString($errno) {
    $type = [
        E_ERROR => 'Fatal error',
        E_RECOVERABLE_ERROR => 'Recoverable error',
        E_WARNING => 'Warning',
        E_PARSE => 'Parsing error',
        E_NOTICE => 'Notice',
        E_STRICT => 'PHP not strict',
        E_DEPRECATED => 'Deprecation',
        E_CORE_ERROR => 'Core error',
        E_CORE_WARNING => 'Core warning',
        E_COMPILE_ERROR => 'Compilation error',
        E_COMPILE_WARNING => 'Compilation warning',
        E_USER_ERROR => 'User error',
        E_USER_WARNING => 'User warning',
        E_USER_NOTICE => 'User notice',
        E_USER_DEPRECATED => 'User deprecation'
    ];
    if (isset($type[$errno])) {
        return $type[$errno];
    }
    return 'Unknown error';
}
function default_error_handler($errno, $errstr, $errfile, $errline) {
    if (isErrorCatchable($errno)) {
        echo '1';
        getErrorPage(null);
        $GLOBALS['alreadyCaught'] = true;
        throw new RuntimeException(getErrorString($errno) . " in $errfile (line $errline) :\r\n$errstr", $errno);
    }
}

function default_shutdown_handler() {
    if (!$GLOBALS['alreadyCaught']) {
        $lastError = error_get_last();
        if ($lastError){
            var_dump($lastError);
            if (isErrorCatchable($lastError['type'])) {
                getErrorPage($lastError);
            }
        }
    }
}

/**
 * Affiche la page d'erreur
 * @param $lastError
 * @throws \Twig\Error\LoaderError
 * @throws \Twig\Error\RuntimeError
 * @throws \Twig\Error\SyntaxError
 */
function getErrorPage($lastError){
    $dir = __DIR__ .'\\mvc\\view\\';
    $file = 'error.html.twig';
    $loader = new FilesystemLoader($dir);
    $twig = new Environment($loader);
    echo $twig->render($file,$lastError);
}
// Handle every errors as Exceptions
// Errors
$errors = E_ERROR | E_RECOVERABLE_ERROR | E_CORE_ERROR | E_COMPILE_ERROR | E_USER_ERROR | E_PARSE;
// Warnings
$errors = $errors | E_WARNING | E_CORE_WARNING | E_COMPILE_WARNING | E_USER_WARNING;
// Notices
$errors = $errors | E_NOTICE | E_USER_NOTICE;
// Strict coding
$errors = $errors | E_STRICT | E_DEPRECATED | E_USER_DEPRECATED;

set_error_handler('default_error_handler', $errors);
register_shutdown_function('default_shutdown_handler');
spl_autoload_register('autoload');
session_start();
function autoload($class){
//    var_dump($class);
    $temp = explode('\\',$class);
    if ($temp[0] == 'mvc'){
        if (file_exists($class.'.php')){
            require_once $class.'.php';
        }
    }
}


try {
    $test = new router();
    $test->getRoute();
}catch (Exception $e){
    getErrorPage([
        'e' => $e
    ]);
}
//    print_r($ex->getMessage());
//var_dump(print_r($GLOBALS));