-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 17 déc. 2020 à 20:42
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database biblotheque
-- --------------------------------------------------------
DROP DATABASE IF EXISTS bibliotheque;
CREATE DATABASE bibliotheque;
USE bibliotheque;
--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id_Article` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle_Article` varchar(255) DEFAULT NULL,
  `reference_Article` varchar(255) DEFAULT NULL,
  `disponible_Article` tinyint(1) DEFAULT NULL,
  `id_typeArticle` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_Article`),
  KEY `FK_article_id_typeArticle` (`id_typeArticle`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id_Article`, `libelle_Article`, `reference_Article`, `disponible_Article`, `id_typeArticle`) VALUES
(13, 'La machine - JUL', 'cd-jl123', 0, 2),
(14, 'Sur la route de Jadiel - PokemonMusic', 'cd-pkmn1645', 1, 2),
(15, 'BLO - 13 block', 'cd-13465', 0, 2),
(16, 'LMF - Freeze Corleone', 'cd-45456', 0, 2),
(17, 'Titanic', 'dvd-4456', 1, 3),
(18, 'Intouchables', 'dvd-454', 0, 3),
(19, 'OSS 117', 'dvd-9657', 1, 3),
(20, 'Avatar', 'dvd-564', 1, 3),
(21, 'Closer n°236', 'mag-544', 1, 4),
(22, 'La redoute', 'mag-6564', 1, 4),
(23, 'Première n°123', 'mag-45', 1, 4),
(24, 'Fluide Glacial', 'mag-564', 1, 4),
(25, 'Les essais - Montaigne', 'lv-54645', 1, 5),
(26, 'Les misérables - Victor Hugo', 'lv-564', 0, 5);

-- --------------------------------------------------------

--
-- Structure de la table `caracteristique`
--

CREATE TABLE IF NOT EXISTS `caracteristique` (
  `id_caracteristique` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle_caracteristique` varchar(255) DEFAULT NULL,
  `id_typeArticle` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_caracteristique`),
  KEY `FK_caracteristique_id_typeArticle` (`id_typeArticle`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `caracteristique`
--

INSERT INTO `caracteristique` (`id_caracteristique`, `libelle_caracteristique`, `id_typeArticle`) VALUES
(3, 'Temps', 2),
(4, 'Label', 2),
(5, 'Réalisateur', 3),
(6, 'Genre', 3),
(7, 'Nombre de pages', 5),
(8, 'Maison d\'édition', 5),
(9, 'Thème', 4),
(10, 'Date', 4);

-- --------------------------------------------------------

--
-- Structure de la table `typearticle`
--

CREATE TABLE IF NOT EXISTS `typearticle` (
  `id_typeArticle` bigint(20) NOT NULL AUTO_INCREMENT,
  `libelle_typeArticle` varchar(255) DEFAULT NULL,
  `empruntable_typeArticle` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_typeArticle`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typearticle`
--

INSERT INTO `typearticle` (`id_typeArticle`, `libelle_typeArticle`, `empruntable_typeArticle`) VALUES
(2, 'CD', 1),
(3, 'DVD', 1),
(4, 'Magazine', 0),
(5, 'Livre', 1);

-- --------------------------------------------------------

--
-- Structure de la table `valuecaracteristique`
--


CREATE TABLE IF NOT EXISTS `valuecaracteristique` (
  `id_valueCaracteristique` bigint(20) NOT NULL AUTO_INCREMENT,
  `Value_valueCaracteristique` varchar(255) DEFAULT NULL,
  `id_caracteristique` bigint(20) DEFAULT NULL,
  `id_Article` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_valueCaracteristique`),
  KEY `FK_valueCaracteristique_id_caracteristique` (`id_caracteristique`),
  KEY `FK_valueCaracteristique_id_Article` (`id_Article`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `valuecaracteristique`
--

INSERT INTO `valuecaracteristique` (`id_valueCaracteristique`, `Value_valueCaracteristique`, `id_caracteristique`, `id_Article`) VALUES
(5, '45:12', 3, 16),
(6, 'Djam', 4, 16),
(7, '16:23', 3, 13),
(8, 'D\'or et de platines', 4, 13),
(9, '50:26', 3, 15),
(10, 'Universal', 4, 15),
(11, '4:26', 3, 14),
(12, 'Pokemon', 4, 14),
(13, 'James Cameron', 5, 20),
(14, 'Action', 6, 20),
(15, 'Comédie', 6, 18),
(16, 'Olivier Nakache', 5, 18),
(17, 'James Cameron', 5, 17),
(18, 'Drame', 6, 17),
(19, '25/09/2017', 10, 22),
(20, 'Vêtements', 9, 22),
(21, '31/12/2019', 10, 21),
(22, 'People', 9, 21),
(23, 'Casterman', 8, 25),
(24, '600', 7, 25),
(25, 'Poche', 8, 26),
(26, '900', 7, 26);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_article_id_typeArticle` FOREIGN KEY (`id_typeArticle`) REFERENCES `typearticle` (`id_typeArticle`);

--
-- Contraintes pour la table `caracteristique`
--
ALTER TABLE `caracteristique`
  ADD CONSTRAINT `FK_caracteristique_id_typeArticle` FOREIGN KEY (`id_typeArticle`) REFERENCES `typearticle` (`id_typeArticle`);

--
-- Contraintes pour la table `valuecaracteristique`
--
ALTER TABLE `valuecaracteristique`
  ADD CONSTRAINT `FK_valueCaracteristique_id_Article` FOREIGN KEY (`id_Article`) REFERENCES `article` (`id_Article`),
  ADD CONSTRAINT `FK_valueCaracteristique_id_caracteristique` FOREIGN KEY (`id_caracteristique`) REFERENCES `caracteristique` (`id_caracteristique`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
