<?php
namespace mvc\utils;

use RuntimeException;

class router
{

    private array $parametres;

    /**
     * router constructor.
     */
    public function __construct()
    {
        $this->parametres = array_merge($_GET,$_POST);
    }

    public function getRoute(){
        $this->getController();
    }

    /**
     * Génère le controller en fonction des paramètre passer dans l'url
     */
    private function getController(){
        // initialization des paramètres pour toujours générer la page d'acueil
        if(!(isset($this->parametres['module']) && ($this->parametres['module'] != ""))){
            $this->parametres['module'] = 'article';
        }
        //Empêche de charger le Acontroller, abstract
        if (strtolower($this->parametres['module']) == 'a'){
            throw new RuntimeException('Page introuvable');
        }
        if (!(isset($this->parametres['page']) && ($this->parametres['page'] != ""))){
            $this->parametres['page'] = 'home';
        }
        $module = ucfirst(strtolower($this->parametres['module']));
        //Récupère le chemin de la classe controller a créer
        $classeControleur = '\\mvc\\controller\\'.$module . 'Controller';
        //si la classe du controller existe créer le controller, sinon throw un execption
        if (class_exists($classeControleur)){
            $controller = new $classeControleur($this->parametres);
        }else{
            throw new RuntimeException("Page introuvable");
        }
        //créer le nom de la fonction qui créera la page en fonction du paramètre dans l'url
        $page = 'get_' . ucfirst($this->parametres['page']);
        //génére la page
        $controller->getView($page);
    }

    public function existeParametre(string $nom) :bool {
        return (isset($this->parametres[$nom]) && ($this->parametres[$nom] != ""));
    }
}