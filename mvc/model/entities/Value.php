<?php


namespace mvc\model\entities;


class Value
{
    private int $id;
    private string $value;
    private Articles $article;
    private Caracteristique $caracteristique;

    /**
     * Value constructor.
     * @param int $id
     * @param string $value
     * @param Articles $article
     * @param Caracteristique $caracteristique
     */
    public function __construct(int $id, string $value, Articles $article, Caracteristique $caracteristique)
    {
        $this->id = $id;
        $this->value = $value;
        $this->article = $article;
        $this->caracteristique = $caracteristique;
    }


    /**
     * @return Articles
     */
    public function getArticle(): Articles
    {
        return $this->article;
    }

    /**
     * @param Articles $article
     */
    public function setArticle(Articles $article): void
    {
        $this->article = $article;
    }

    /**
     * @return Caracteristique
     */
    public function getCaracteristique(): Caracteristique
    {
        return $this->caracteristique;
    }

    /**
     * @param Caracteristique $caracteristique
     */
    public function setCaracteristique(Caracteristique $caracteristique): void
    {
        $this->caracteristique = $caracteristique;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }


}