<?php

namespace mvc\model\entities;

class TypeArticles
{
    private int $id;
    private string $libelle;
    private bool $empruntable;

    /**
     * typeArticle constructor.
     * @param int $id
     * @param string $libelle
     * @param bool $empruntable
     */
    public function __construct(int $id, string $libelle, bool $empruntable)
    {
        $this->id = $id;
        $this->libelle = $libelle;
        $this->empruntable = $empruntable;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return bool
     */
    public function isEmpruntable(): bool
    {
        return $this->empruntable;
    }

    /**
     * @param bool $empruntable
     */
    public function setEmpruntable(bool $empruntable): void
    {
        $this->empruntable = $empruntable;
    }

    public function __toString(): string
    {
        return $this->getLibelle();
    }


}