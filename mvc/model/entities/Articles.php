<?php
namespace mvc\model\entities;


class Articles{

    private int $id;
    private string $libelle;
    private string $ref;
    private bool $disponible;
    private TypeArticles $typeArticle;
    private array $caracteristiques;

    /**
     * Articles constructor.
     * @param int $id
     * @param string $libelle
     * @param string $ref
     * @param bool $disponible
     * @param TypeArticles $typeArticle
     */
    public function __construct(int $id, string $libelle, string $ref, bool $disponible, TypeArticles $typeArticle)
    {

        $this->id = $id;
        $this->libelle = $libelle;
        $this->ref = $ref;
        $this->disponible = $disponible;
        $this->typeArticle = $typeArticle;

    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getRef(): string
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     */
    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }

    /**
     * @return bool
     */
    public function isDisponible(): bool
    {
        return $this->disponible;
    }

    /**
     * @param bool $disponible
     */
    public function setDisponible(bool $disponible): void
    {
        $this->disponible = $disponible;
    }

    /**
     * @return TypeArticles
     */
    public function getTypeArticle(): TypeArticles
    {
        return $this->typeArticle;
    }

    /**
     * @param TypeArticles $typeArticle
     */
    public function setTypeArticle(TypeArticles $typeArticle): void
    {
        $this->typeArticle = $typeArticle;
    }

    public function __toString(): string
    {
        return $this->getLibelle();
    }

    /**
     * @return array
     */
    public function getCaracteristiques(): array
    {
        return $this->caracteristiques;
    }

    /**
     * @param array $caracteristiques
     */
    public function setCaracteristiques(array $caracteristiques): void
    {
        $this->caracteristiques = $caracteristiques;
    }


}