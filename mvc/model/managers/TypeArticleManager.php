<?php
namespace mvc\model\managers;

use mvc\model\entities\TypeArticles;

class TypeArticleManager extends PDOManager
{

    public function findById(int $id): ?TypeArticles
    {
        $stmt = $this->executePrepare("select * from typearticle where id_typeArticle=:id", [ "id" => $id]);
        $typeArticle = $stmt->fetch();
        if (!$typeArticle) return null;
        return new TypeArticles($typeArticle["id_typeArticle"],$typeArticle["libelle_typeArticle"],$typeArticle["empruntable_typeArticle"] == 1);
    }

    public function find(): \PDOStatement
    {
        $stmt=$this->executePrepare("select * from typearticle",[]);
        return $stmt;
    }

    public function findAll(): array
    {
        $stmt=$this->find();
        $typeArticles = $stmt->fetchAll();

        $typeArticleEntities=[];

        foreach($typeArticles as $typeArticle) {
            $typeArticleEntities[] = new TypeArticles(intval($typeArticle["id_typeArticle"]),$typeArticle["libelle_typeArticle"], $typeArticle['empruntable_typeArticle'] == 1);
        }
        return $typeArticleEntities;
    }
}
