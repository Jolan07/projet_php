<?php
namespace mvc\model\managers;

require_once 'conf/database.php';
use PDO;
use PDOStatement;

abstract class PDOManager
{
    private string $host, $db, $encoding, $user, $pass;
    private int $pdoErrorMode;
    private int $LastIdInsert;

    /**
     * Manager constructor
     */
    public function __construct()
    {
        $this->host = strval($GLOBALS["host"]);
        $this->db = strval($GLOBALS["db"]);
        $this->encoding = strval($GLOBALS["encoding"]);
        $this->user = strval($GLOBALS["user"]);
        $this->pass = strval($GLOBALS["pass"]);
        $this->pdoErrorMode = (int) $GLOBALS["pdoErrorMode"];

    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getDb(): string
    {
        return $this->db;
    }

    /**
     * @param string $db
     */
    public function setDb(string $db): void
    {
        $this->db = $db;
    }

    /**
     * @return string
     */
    public function getEncoding(): string
    {
        return $this->encoding;
    }

    /**
     * @param string $encoding
     */
    public function setEncoding(string $encoding): void
    {
        $this->encoding = $encoding;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user): void
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass(string $pass): void
    {
        $this->pass = $pass;
    }

    /**
     * @return int
     */
    public function getPdoErrorMode(): int
    {
        return $this->pdoErrorMode;
    }

    /**
     * @param int $pdoErrorMode
     */
    public function setPdoErrorMode(int $pdoErrorMode): void
    {
        $this->pdoErrorMode = $pdoErrorMode;
    }

    protected function dbConnect() : PDO
    {
        $conn = new PDO("mysql:host=$this->host;dbname=$this->db;charset=$this->encoding", $this->user, $this->pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, $this->pdoErrorMode);
        return $conn;
    }


    protected function executePrepare(string $req, array $params) : PDOStatement {
        $conn = null;
        try {
            $conn = $this->dbConnect();
            $stmt = $conn->prepare($req);
            $res = $stmt->execute($params);
            $this->LastIdInsert = $conn->lastInsertId();
            return $stmt;
        }
        catch (PDOException $ex) {
            //echo "Error ".$ex->getCode()." : ".$ex->getMessage()."<br/>".$ex->getTraceAsString();
            throw $ex;
        }
        finally {
            if ($conn != null) {
                $conn = null;
            }
        }
    }

    /**
     * @return int
     */
    public function getLastIdInsert(): int
    {
        return $this->LastIdInsert;
    }


}