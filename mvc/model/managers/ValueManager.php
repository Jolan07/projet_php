<?php


namespace mvc\model\managers;


use mvc\model\entities\Articles;
use mvc\model\entities\Value;

//Correspond à la table valuecaractéristiques de la base de données
class ValueManager extends PDOManager
{

    //La valeur d'une caractéristique est associé à un article, un article ne peut avoir qu'une seule valeur pour un caractéristique donné, mais plusieurs caractéristiques (ex: Titanic : Réalisateur = Cameron, Genre = drame)

    //Cette fonction récupère toutes les caractéristiques et leurs valeurs d'un article
    public function findValueCaracterisqueByArticle(Articles $article): array
    {
        $idArticle = $article->getId();
        $stmt = $this->executePrepare("SELECT Value_valueCaracteristique, id_valueCaracteristique, id_caracteristique FROM valuecaracteristique WHERE  id_Article=:id", [ "id" => $idArticle]);
        $caracteristiques = $stmt->fetchAll();
        $ListCaracteristique = [];
        $managerCarac = new CaracteristiqueManager();
        //Création et renvoi d'objets caractéristiques qui seront liés à un objet article
        foreach($caracteristiques as $caracteristique) {
            $carac = $managerCarac->findCaracterisqueById($caracteristique["id_caracteristique"]);
            $ListCaracteristique[] = new Value($caracteristique["id_valueCaracteristique"], $caracteristique["Value_valueCaracteristique"], $article, $carac);
        }
        return $ListCaracteristique;
    }

    //Création de la valeur d'une caractéristique d'un article
    public function insertValueCaracteristique(int $idArticle, string $value, int $id_caracteristique): \PDOStatement
    {
        $res = $this->executePrepare("INSERT INTO valuecaracteristique (Value_valuecaracteristique,id_caracteristique,id_Article)
                                       VALUES (:value,:idCar,:idArticle)", [ "value" => $value,"idCar" => $id_caracteristique,"idArticle" => $idArticle]);
        return $res;
    }

    //Modification d'une valeur d'une caractéristique d'un article
    public function updateValueCaracteristique( string $value, int $idValue): \PDOStatement
    {
        $res = $this->executePrepare("UPDATE valuecaracteristique SET Value_valuecaracteristique = :value WHERE id_valueCaracteristique = :idValue"
        , [ "value" => $value, "idValue" => $idValue]);
        return $res;
    }

    //Suppression d'une valeur caractéristique, s'execute lorsqu'on supprime un article
    public function deleteValueCaracteristiqueByArticle(int $idArticle): \PDOStatement
    {
        $res = $this->executePrepare("DELETE FROM valuecaracteristique where id_Article=:id", [ "id" => $idArticle]);
        return $res;
    }
}