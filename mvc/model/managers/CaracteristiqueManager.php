<?php


namespace mvc\model\managers;


use mvc\model\entities\Articles;
use mvc\model\entities\Caracteristique;

class CaracteristiqueManager extends PDOManager
{

    //Une caractéristique est lié à un type d'article (ex: temps et label pour les cds), un type d'article peut avoir un nombre infini de caractéristiques
    public function findCaracterisqueByType(int $idType): array
    {
        $stmt = $this->executePrepare("SELECT libelle_caracteristique, id_caracteristique FROM caracteristique where id_typeArticle=:id", [ "id" => $idType]);
        $caracteristiques = $stmt->fetchAll();
        $ListCaracteristique = [];
        foreach($caracteristiques as $caracteristique) {
            $ListCaracteristique[] = new Caracteristique($caracteristique["id_caracteristique"], $caracteristique["libelle_caracteristique"]);
        }
        return $ListCaracteristique;
    }

    public function findCaracterisqueById(int $id): ?Caracteristique
    {
        $stmt = $this->executePrepare("SELECT libelle_caracteristique, id_caracteristique FROM caracteristique where id_caracteristique=:id", [ "id" => $id]);
        $res = $stmt->fetch();
        if (!$res){
            return null;
        }
        $caracteristiques = new Caracteristique($res["id_caracteristique"], $res["libelle_caracteristique"]);
        return $caracteristiques;
    }



}