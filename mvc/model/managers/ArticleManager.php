<?php

namespace mvc\model\managers;


use mvc\model\entities\Articles;
use PDO;
use PDOStatement;


class ArticleManager extends PDOManager
{
    //Création d'un array Caractéristiques lié à l'objet Article permettant d'afficher dynamiquement toutes les caractéristiques et ses valeurs de l'article
    public function findById(int $id): ?Articles
    {
        //Préparation de la requête qui récupère l'article en fonction de son id
        $stmt = $this->executePrepare("select * from article where id_Article=:id", [ "id" => $id]);
        $article = $stmt->fetch();
        if (!$article) return null;

        $typeArticleManger = new TypeArticleManager();
        $caracteristiqueManager = new ValueManager();
        //Création d'un objet article avec son typeArticle associés et ses caractéristiques
        $type = $typeArticleManger->findById(intval($article["id_typeArticle"]));
        $article = new Articles(intval($article["id_Article"]),$article["libelle_Article"],$article["reference_Article"],$article["disponible_Article"] == "1" ,$type);
        $article->setCaracteristiques($caracteristiqueManager->findValueCaracterisqueByArticle($article));
        return $article;
    }

    public function find(): PDOStatement
    {
        $stmt=$this->executePrepare("select * from article",[]);
        return $stmt;
    }

    public function findAll(): array
    {
        $stmt=$this->find();
        $articles = $stmt->fetchAll();
        $articleEntities=[];
        $typeArticleManger = new TypeArticleManager();
        $caracteristiqueManager = new ValueManager();
        //Création d'un tableau qui renvoie tous les articles de la base de données
        foreach($articles as $article) {
            $type = $typeArticleManger->findById(intval($article["id_typeArticle"]));
            $article =  new Articles(intval($article["id_Article"]),$article["libelle_Article"],$article["reference_Article"],$article["disponible_Article"] == "1" ,$type);
            $article->setCaracteristiques($caracteristiqueManager->findValueCaracterisqueByArticle($article));
            $articleEntities[] = $article;
        }

        return $articleEntities;
    }

      //Création d'un article dans la base de données
      public function insertArticle(Articles $article): int
       {
            $req = "insert into article(libelle_Article, reference_Article, disponible_Article, id_typeArticle) values (:libelle, :reference, :disponible, :typeArticle)";
            $params = array("libelle" => $article->getLibelle(), "reference" => $article->getRef(), "disponible" => $article->isDisponible()?1:0, "typeArticle" => $article->getTypeArticle()->getId());
            $res = $this->executePrepare($req,$params);
            return $this->getLastIdInsert();
       }

    //Modification d'un article dans la base de données
    public function updateArticle(Articles $article): PDOStatement
    {
        $req = "UPDATE article SET libelle_Article = :libelle, reference_Article = :reference, disponible_Article = :disponible, id_typeArticle = :typeArticle WHERE id_article = :id";
        $params = array("libelle" => $article->getLibelle(), "reference" => $article->getRef(), "disponible" => $article->isDisponible()?1:0, "typeArticle" => $article->getTypeArticle()->getId(), "id" => $article->getId());
        $res = $this->executePrepare($req,$params);
        return $res;
    }


    //Suppression d'un article
    public function deleteArticle(int $id): PDOStatement
    {
        $managerCar = new ValueManager();
        $managerCar->deleteValueCaracteristiqueByArticle($id);
        $req = "DELETE FROM article WHERE id_Article = :id";
        $params = array("id" => $id);
        $res = $this->executePrepare($req,$params);
        return $res;
    }

    //Récupération d'un article en fonction de son type dans la base de données
    public function findByType(int $idType): array
    {
        $stmt = $this->executePrepare("select * from article where id_typeArticle=:idType ", [ "idType" => $idType]);
        $articles = $stmt->fetchAll();
        $articleEntities=[];
        $typeArticleManager = new TypeArticleManager();
        foreach($articles as $article) {
            $type = $typeArticleManager->findById(intval($idType));
            $articleEntities[] = new Articles(intval($article["id_Article"]),$article["libelle_Article"],$article["reference_Article"],$article["disponible_Article"] == "1" ,$type);
        }
        return $articleEntities;
    }
}
