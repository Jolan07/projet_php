<?php
namespace mvc\controller;

use mvc\model\entities\Articles;
use mvc\model\entities\Caracteristique;
use mvc\model\managers\ArticleManager;
use mvc\model\managers\CaracteristiqueManager;
use mvc\model\managers\TypeArticleManager;
use mvc\model\managers\ValueManager;
use PDO;

class ArticleController extends AController
{
    public function __construct(array $param)
    {
        parent::__construct($param);
    }


    public function get_Home() :array{
        $typeManager = new TypeArticleManager();
        $typeArticles = $typeManager->findAll(); //recuperation de tout les articles
        return array(
            'typeArticles' => $typeArticles,
        );
    }

//Gere la recupération et l'affiche des article pour un Type d'article
    public function get_Articles() :array{
        $managerType = new TypeArticleManager();
        $type = $managerType->findById($this->getParam('id'));
        //Redirection si aucun Type n'existe pour cet ID
        if (!$type){
            header('Location: ?page=Home&module=Article');
        }
        $articles = $this->manager->findByType($this->getParam('id'));
        return array(
            'articles' => $articles,
            'typeArticle' => $type
        );
    }

    //Gere la recupération et l'affiche d'un article
    public function get_Article() :array{
        $article = $this->manager->findById($this->getParam('id'));
        //Redirection si aucun Article n'existe pour cet ID
        if (!$article){
            header('Location: ?page=Home&module=Article');
        }
        $managerCarac = new ValueManager();
        $caracteristiques = $managerCarac->findValueCaracterisqueByArticle($article);
        return array(
            'article' => $article
        );
    }

    // Gère la suppression d'un article
    public function get_Delete(){
        $article = $this->manager->findById($this->getParam('id'));
        //Redirection si aucun Article n'existe pour cet ID
        if (!$article){
            header('Location: ?page=Home&module=Article');
        }
        $this->manager->deleteArticle($this->getParam('id'));
        $idType = $article->getTypeArticle()->getId();
        header('Location: ?page=Articles&module=Article&id=' . $idType);
    }

    //Gère l'ajout et la mofification d'un Article et ces caractéristiques
    public function get_Formajout(){
        //initialisation des variable dont on aura besoin
        // dans l'ensemble des cas
        $managerType = new TypeArticleManager();
        $idType = $this->getParam('id');
        $type =  $managerType->findById($idType);
        //Redirection si aucun type n'existe pour cet ID
        if (!$type){
            header('Location: ?page=Home&module=Article');
        }
        $managerCarac = new CaracteristiqueManager();
        $caracteristiques = $managerCarac->findCaracterisqueByType($idType);

        // S'il existe un paramètre idArticle
        // et qu'un article avec cette id existe dans la BDD alors
        // il va s'agir d'un modif
        if ($this->existeParam('idArticle') && $this->manager->findById($this->getParam('idArticle'))){
            $article = $this->manager->findById($this->getParam('idArticle'));
            //Si formaulaire submit
            if ($this->existeParam('libelle') &&  $this->existeParam('ref')){
                //initialisation des nouvelle valeur de l'article recupéré depuis le form
                $article->setLibelle($this->getParam('libelle'));
                $article->setRef($this->getParam('ref'));
                $article->setDisponible($this->getParam('disponible'));
                //Update en BDD
                $this->manager->updateArticle($article);

                // Modification en BDD des valeur des caracteriqtique special de l'article
                $valueManger = new ValueManager();
                foreach ($article->getCaracteristiques() as $caracteristique){
                    $typeCarac = $caracteristique->getCaracteristique();
                    $param = str_replace(' ', '_', $typeCarac->getLibelle());
                    $param = trim($this->getParam($param));
                    $valueManger->updateValueCaracteristique($param,$caracteristique->getId());
                }
                //Redirection vers la vers la page catégorie de l'article
                header('Location: ?page=Articles&module=Article&id=' . $idType);
            }else{
                return [
                    'article'=> $article
                ];
            }
        //sinon il s'agit d'un ajout
        }else{
            // Si le formaulaire est submit
            if ($this->existeParam('libelle') &&  $this->existeParam('ref') ){
                // creation d'un objet Article avec les valeur du form
                $article = new Articles(1,$this->getParam('libelle'),$this->getParam('ref'),1,$type);
                //Insertion dans la bdd et recuperation de son ID
                $idArticle = $this->manager->insertArticle($article);
                $_SESSION['libelle' . $type->getId()] = $this->getParam('libelle');
                $_SESSION['ref'. $type->getId()] = $this->getParam('ref');
                //Insertion des caracterisques de l'article avec les valeur du form
                $valueManger = new ValueManager();
                foreach ($caracteristiques as $caracteristique){
                    $param = str_replace(' ', '_', $caracteristique->getLibelle());
                    $param = trim($this->getParam($param));
                    $_SESSION[$caracteristique->getLibelle() . $type->getId()] = $param;
                    $valueManger->insertValueCaracteristique($idArticle,$param,$caracteristique->getId());
                }
                header('Location: ?page=Articles&module=Article&id=' . $idType);
            //Sinon
            }else{
                //On passe les caractérisques liée au type de l'article
                // afin de pouvoir genérer dynamiquement le formulaire d'ajout
                return array(
                    'caracteristiques' => $caracteristiques,
                    'type' => $type,
                    'session' => $_SESSION
                );
            }
        }
    }

}