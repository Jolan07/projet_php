<?php
namespace mvc\controller;

use Exception;
use RuntimeException;
use mvc\model\managers\PDOManager;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;


abstract class AController
{
    private array $param;
    protected PDOManager $manager;

    /**
     * AController constructor.
     * @param array $param
     */
    public function __construct(array $param)
    {
        $this->param = $param;
        //Le nom du controller de l'instance
        $className = $this->getNameClass();
        $classManager = '\\mvc\\model\\managers\\'. $className . 'Manager';
        if (class_exists($classManager)){
            $this->manager = new $classManager();
        }else{
            throw new RuntimeException("Page introuvable");
        }
    }

    /**
     * Renvoie le nom de la classe php reformatée, exemple Classe= ArticleController, renvoie Article
     * @return string
     */
    private function getNameClass(): string
    {
        $class = explode("\\",get_class($this));
        $class = $class[count($class)-1];
        $class =  preg_split('/(?=[A-Z])/', $class)[1];
        return $class;
    }

    /**
     * Affiche lla page passer en paramètre
     * @param string $pageToRender
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public final function getView(string $pageToRender){
            $class = $this->getNameClass();
            //Si la fonction de la page n'existe pas, throw une erreur
            if (!method_exists($this,$pageToRender)){
                throw new RuntimeException("Page introuvable");
            }
            //Récupère les variables de la page
            $variables = $this->$pageToRender();
            $variables = array_merge($variables,["session" => $_SESSION]);
            //dossiers des vues
            $dir = 'mvc/view' ;
            //initialization de twig
            $file = $this->param['page'] .'.html.twig';
            $loader = new FilesystemLoader($dir);
            $twig = new Environment($loader);
            //affiche la page, et passe en paramètre les variable pour la page
            echo $twig->render($class . '/'. $file,$variables);

    }

    /**
     * Renvoie un paramètre, si il n'existe pas throw une execution
     * @param $name : string
     * @return string
     */
    protected function getParam(string $name) :string{
        if ($this->existeParam($name)){
            return $this->param[$name];
        }else{
            throw new RuntimeException("Paramètre " . $name . " n'existe pas");
        }
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function existeParam(string $name):bool{
        return isset($this->param[$name]) && trim($this->param[$name]) != "";
    }
    /**
     * page par defaut de chaque controller
     * @return array|null
     */
    public abstract function get_Home() :?array;
}